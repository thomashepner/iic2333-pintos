/* Verifies that lowering a thread's priority so that it is no
   longer the highest-priority thread in the system causes it to
   yield immediately. */

#include <stdio.h>
#include "tests/threads/tests.h"
#include "threads/init.h"
#include "threads/thread.h"

static thread_func changing_thread;

void
test_priority_change (void)
{
  /* This test does not work with the MLFQS. */
  ASSERT (selected_scheduler != SCH_MLFQS);

  printf("Creando threads \n");
  thread_create ("thread 10", PRI_DEFAULT -1, changing_thread, NULL);
  //printf("Ya se debe haber ejecutado el thread 10 (expropiacion)\n");
  thread_create ("thread 11", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 12", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 13", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 14", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 15", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 16", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 17", PRI_DEFAULT -1, changing_thread, NULL);

  while (true)
  {
  	//do nothing

  }

  //msg ("Thread 2 should have just lowered its priority.");
  //thread_set_priority (PRI_DEFAULT - 2);
  //msg ("Thread 2 should have just exited.");
}

static void
changing_thread (void *aux UNUSED)
{

	while (true)
	{
		//do nothing
	}
}
/* Checks that when the alarm clock wakes up threads, the
   higher-priority threads run first. */

#include <stdio.h>
#include "tests/threads/tests.h"
#include "threads/init.h"
#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "devices/timer.h"

static thread_func changing_thread;
//static int64_t wake_time;
//static struct semaphore wait_sema;

void
test_alarm_priority (void)
{
  int i;

  /* This test does not work with the MLFQS. */
  ASSERT (selected_scheduler != SCH_MLFQS);

  printf("Creando threads \n");
  thread_create ("thread 10", PRI_DEFAULT -1, changing_thread, NULL);
  //printf("Ya se debe haber ejecutado el thread 10 (expropiacion)\n");
  thread_create ("thread 11", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 12", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 13", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 14", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 15", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 16", PRI_DEFAULT -1, changing_thread, NULL);
  thread_create ("thread 17", PRI_DEFAULT -1, changing_thread, NULL);

  for (i=0; i<10000000; i++)
  {

  }

  //msg ("Thread 2 should have just lowered its priority.");
  //thread_set_priority (PRI_DEFAULT - 2);
  //msg ("Thread 2 should have just exited.");
}

static void
changing_thread (void *aux UNUSED)
{
  int i;

  for (i=0; i<1000; i++)
  {

  }

}